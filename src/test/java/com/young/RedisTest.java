package com.young;

import com.young.pojo.User;
import com.young.util.RedisUtil;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = YoungApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class RedisTest {

    @Resource
    private RedisUtil redisUtil;

    @Test
    public void redisTest(){
        User user=new User();
        user.setOpenid("1121");
        redisUtil.setUser("u1",user);
        User res=redisUtil.getUser("100");
        System.out.println(res);
    }
}
