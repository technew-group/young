package com.young;

import com.young.util.HttpUtil;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * http请求发送测试类
 *
 * @author 吕圣业
 * @since 3 六月 2019
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class HttpUtilTest {

    @Test
    public void httpTest(){
        //发送 GET 请求
        String s= HttpUtil.sendGet("http://v.qq.com/x/cover/kvehb7okfxqstmc.html?vid=e01957zem6o", "");
        System.out.println(s);

//        //发送 POST 请求
//        String sr=HttpUtil.sendPost("http://www.toutiao.com/stream/widget/local_weather/data/?city=%E4%B8%8A%E6%B5%B7", "");
//        JSONObject json = JSONObject.fromObject(sr);
//        System.out.println(json.get("data"));
    }
}
