package com.young;

import com.young.mapper.UserMapper;
import com.young.pojo.Position;
import com.young.pojo.User;
import com.young.pojo.UserExample;
import com.young.service.CompanyService;
import com.young.service.EsService;
import com.young.service.PositionService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {YoungApplication.class}, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class YoungApplicationTests {

    @Test
    public void contextLoads() {
        System.setProperty("es.set.netty.runtime.available.processors", "false");
    }

    @Autowired
    EsService esService;

//    @Test
//    public void esTest() throws IOException {
//        List<JSONObject> result = esService.getPosition("哈尔滨");
//        System.out.println("-----------------");
//        System.out.println(result);
//    }


    @Autowired
    PositionService positionService;
    @Autowired
    CompanyService companyService;

    @Test
    public void test() {
        int page = 1;
        List<Position> positionListForPage = positionService.getPositionListForPage(1);


        System.out.println("------------------------------------");
        for (Position position : positionListForPage) {
            System.out.println(position);
        }
        System.out.println("营地职位信息-----------------------------------");
        Position position1 = positionService.getPosition(5);
        System.out.println(position1);
        System.out.println("公司-----------------------------");
        System.out.println(companyService.getCompanyById(5));
    }

    @Resource
    UserMapper userMapper;

    @Test
    public void userTest() {
        UserExample example = new UserExample();
        UserExample.Criteria criteria = example.createCriteria();
        criteria.andOpenidEqualTo("bT1Jiqlniu6f6D0gzjLzSQ");
        List<User> users = userMapper.selectByExample(example);
        System.out.println(users.size());
    }
}
