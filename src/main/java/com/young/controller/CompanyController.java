package com.young.controller;

import com.young.pojo.Company;
import com.young.service.CompanyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author 张龙龙
 * @date 2019/6/2 20:41
 *
 *
 * 获取营地公司 控制类  支持restful风格
 */

@RestController
@RequestMapping("company")
public class CompanyController {
    @Autowired
    CompanyService companyService;

    @RequestMapping("getCompany/{cid}")
    public Company getCompany(@PathVariable("cid") Integer cid){ return companyService.getCompanyById(cid);}
}
