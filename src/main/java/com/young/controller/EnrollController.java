package com.young.controller;

import com.young.pojo.Enroll;
import com.young.service.EnrollService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 我的报名，报名
 *
 * @author 吕圣业
 * @since 2 六月 2019
 */
@RestController
@RequestMapping("enrolls")
public class EnrollController {

    @Autowired
    EnrollService enrollService;

    /**
     * 已投递
     * @param uid
     * @return
     */
    @RequestMapping(value = "/1/{uid}",method = RequestMethod.GET)
    public List<Enroll> getAllDeliver(@PathVariable int uid){
        List<Enroll> enrolls=enrollService.getAllDeliverByUid(uid);
        return enrolls;
    }

    /**
     * 已查看
     * @param uid
     * @return
     */
    @RequestMapping(value = "/2/{uid}",method = RequestMethod.GET)
    public List<Enroll> getSaw(@PathVariable int uid){
        List<Enroll> enrolls=enrollService.getSawByUid(uid);
        return enrolls;
    }

    /**
     * 用户是否已报名该职位
     * @param uid
     * @return
     */
    @RequestMapping(value = "/isEnroll/{pid}/{uid}",method = RequestMethod.GET)
    public boolean isEnrolled(@PathVariable int pid,@PathVariable int uid){

        boolean res=enrollService.isEnrolled(pid,uid);
        return res;
    }

    /**
     * 我要报名
     * @param pid
     * @param uid
     * @return
     */
    @RequestMapping(value = "/willingToEnroll/{pid}/{uid}",method = RequestMethod.POST)
    public String putEnroll(@PathVariable int pid,@PathVariable int uid){
        String res=enrollService.addEnroll(pid,uid);
        return res;
    }
}
