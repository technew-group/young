package com.young.controller;

import com.young.pojo.Position;
import com.young.service.PositionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author 张龙龙
 * @date 2019/6/2 16:01
 *
 *
 *  首页返回营地的信息  支持restful风格
 */
@RestController
@RequestMapping("position")
public class PositionController {

    @Autowired
    PositionService positionService;

    /**
     * @param page
     * @return
     *
     * 此方法 查询营地信息(分页  默认大小为8  可以在application.yml中修改默认大小)
     */
    @RequestMapping("getPositionOfPage/{page}")
    public List<Position> getPositionListForPage(@PathVariable("page") Integer page){ return positionService.getPositionListForPage(page);}


    /**
     * @param pid
     * @return
     *
     * 此方法 查询营地的详细信息
     */
    @RequestMapping("getPosition/{pid}")
    public Position  getPosition(@PathVariable("pid") int pid){return positionService.getPosition(pid);}

    /**
     *
     * @return
     *
     * 此方法 查询报名人数最多的8条营地信息
     */
    @RequestMapping("getPosition/search")
    public List<Position>  getPosition(){return positionService.getPositionOfSearch();}
}
