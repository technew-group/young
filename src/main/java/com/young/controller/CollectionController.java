package com.young.controller;

import com.young.pojo.Position;
import com.young.service.CollectionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 我的收藏
 *
 * @author 吕圣业
 * @since 2 六月 2019
 */
@RestController
@RequestMapping("collections")
public class CollectionController {

    @Autowired
    CollectionService collectionService;

    /**
     * 获取所有收藏职位
     *
     * @param uid
     * @return
     */
    @RequestMapping(value = "/{uid}", method = RequestMethod.GET)
    public List<Position> getAllCollection(@PathVariable int uid) {
        List<Position> positions = collectionService.getAllCollectionByUid(uid);
        return positions;
    }

    /**
     * 用户是否已收藏当前职位
     *
     * @param uid
     * @param pid
     * @return
     */
    @RequestMapping(value = "/{uid}/{pid}", method = RequestMethod.GET)
    public boolean getAllCollection(@PathVariable int uid, @PathVariable int pid) {
        boolean res = collectionService.isCollected(uid, pid);
        return res;
    }

    /**
     * 添加收藏
     *
     * @param uid
     * @param pid
     * @return
     */
    @RequestMapping(value = "/{uid}/{pid}", method = RequestMethod.POST)
    public String putCollection(@PathVariable int uid, @PathVariable int pid) {
        String res = collectionService.addCollection(uid, pid);
        return res;
    }

    /**
     * 取消收藏
     *
     * @param uid
     * @param pid
     */
    @RequestMapping(value = "/{uid}/{pid}", method = RequestMethod.DELETE)
    public String deleteCollection(@PathVariable int uid, @PathVariable int pid) {
        String res = collectionService.deleteCollection(uid, pid);
        return res;
    }
}
