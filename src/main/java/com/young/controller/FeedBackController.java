package com.young.controller;

import com.young.pojo.FeedBack;
import com.young.service.FeedBackService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 意见反馈
 *
 * @author 吕圣业
 * @since 2 六月 2019
 */
@RestController
@RequestMapping("/feedBacks")
public class FeedBackController {

    @Autowired
    FeedBackService feedBackService;

    /**
     * 提交意见
     *
     * @param feedBack
     * @return
     */
    @RequestMapping(value = "/", method = RequestMethod.POST)
    public String suggestion(FeedBack feedBack) {
        String res = feedBackService.addSuggestion(feedBack);
        return res;
    }

}
