package com.young.controller;

import com.alibaba.fastjson.JSONObject;
import com.young.biz.UserInfoBiz;
import com.young.pojo.User;
import com.young.pojo.Work_experience;
import com.young.service.UserService;
import com.young.service.VXService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;


/**
 * “我的”页面，调用微信接口
 *
 * @author 吕圣业
 * @since 2 六月 2019
 */
@RestController
@RequestMapping("users")
public class UserController {

    @Autowired
    private UserService userService;
    @Autowired
    private UserInfoBiz userInfoBiz;
    @Autowired
    private VXService vxService;

//    /**
//     * 刚进入“我的简历”时调用
//     * 查用户，返回所有信息
//     * @param openid
//     * @return
//     */
//    @RequestMapping(value = "/{openid}",method = RequestMethod.GET)
//    public User getUser(@PathVariable String openid){
//        User res=userService.userAccess(openid,null);
//        return res;
//    }

    /**
     * 刚进入“我的简历”时调用
     * 查用户，返回所有信息
     * @param uid
     * @return
     */
    @RequestMapping(value = "/{uid}",method = RequestMethod.GET)
    public User getUser(@PathVariable int uid){
        User res=userService.getUserByUid(uid);
        return res;
    }

    /**
     * 修改特长爱好
     * @param basicUserJson
     * @return
     */
    @RequestMapping(value="/favor/modify",method = RequestMethod.POST)
    public String postHobby(String basicUserJson){
        User basicUser=JSONObject.parseObject(basicUserJson,User.class);
        String res=userService.updateBasicInformation(basicUser);
        return res;
    }

    /**
     * 修改基本信息
     * @param basicUserJson
     */
    @RequestMapping(value="/basicUser/modify",method = RequestMethod.POST)
    public String postBasicInformation(String basicUserJson){
        User basicUser=JSONObject.parseObject(basicUserJson,User.class);
        String res=userService.updateBasicInformation(basicUser);
        return res;
    }

    /**
     * 修改经历介绍
     * @param experienceJson
     * @return
     */
    @RequestMapping(value="/workList/modify",method = RequestMethod.POST)
    public String postExperience(String experienceJson){
        Work_experience experience=JSONObject.parseObject(experienceJson,Work_experience.class);
        String res=userService.updateExperience(experience);
        return res;
    }


    /**
     * 获取用户VX信息
     * @param encryptedData
     * @param iv
     * @param code
     * @return
     */
    @RequestMapping(value = "/getUserInfo", method = RequestMethod.GET)
    public Map decodeUserInfo(String encryptedData, String iv, String code) {
        Map map=userInfoBiz.getUserInfoAndCheck(encryptedData,iv,code);
        System.out.println(map);
        return map;
    }

    /**
     * 删除本条经历
     * @param id
     * @return
     */
    @RequestMapping(value = "/workList/{id}/{uid}",method = RequestMethod.DELETE)
    public String deleteExperience(@PathVariable int id,@PathVariable int uid) {
        String res=userService.deleteExperience(id,uid);
        return res;
    }

    /**
     * 获取用户手机号
     * @param encryptedData
     * @param iv
     * @param code
     * @return
     */
    @RequestMapping(value = "get2", method = RequestMethod.POST)
    public void decodeTel(String encryptedData, String iv, String code) {
        vxService.getTelNum(encryptedData,iv,code);
    }

    /**
     * 获取当前用户的经历介绍
     * @param uid
     * @return
     */
    @RequestMapping(value = "/workList/{uid}",method = RequestMethod.GET)
    public List<Work_experience> getWorkExperience(@PathVariable int uid){
        List<Work_experience> experienceList=userService.getWorkExperienceByUid(uid);
        return experienceList;
    }

}
