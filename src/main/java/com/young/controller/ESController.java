package com.young.controller;

import com.alibaba.fastjson.JSONObject;
import com.young.pojo.Position;
import com.young.service.EsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.List;

/**
 * @author 张龙龙
 * @date 2019/6/1 21:11
 *
 * ES 控制类  支持restful风格
 */
@RestController
@RequestMapping("es")
public class ESController {

    @Autowired
    EsService esService;

    /**
     * @param param
     * @return
     *
     * 小程序前端搜索框输入参数 ( 公司名 \ 职位名 \ 地址名)
     *
     * 返回对应的职位信息
     */
    @RequestMapping("getPosition/{param}")
    public List<JSONObject> getPosition(@PathVariable("param") String param) throws IOException { return  esService.getPosition(param); }
}
