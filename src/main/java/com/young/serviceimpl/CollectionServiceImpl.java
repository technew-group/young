package com.young.serviceimpl;

import com.young.mapper.CollectMapper;
import com.young.mapper.PositionMapper;
import com.young.pojo.*;
import com.young.service.CollectionService;
import com.young.util.RedisUtil;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * 提供我的收藏服务
 *
 * @author 吕圣业
 * @since 2 六月 2019
 */
@Service("collectionService")
public class CollectionServiceImpl implements CollectionService {

    private static final Logger LOGGER = LoggerFactory.getLogger(CollectionServiceImpl.class);
    @Resource
    CollectMapper collectMapper;
    @Resource
    PositionMapper positionMapper;
    @Resource
    RedisUtil redisUtil;

    @Override
    public List<Position> getAllCollectionByUid(int uid) {

        List<Position> positions = new ArrayList<>();
        if (StringUtils.isNotBlank(String.valueOf(uid))) {

            positions = redisUtil.getCollect(String.valueOf(uid));
            if (null == positions) {
                CollectExample example = new CollectExample();
                CollectExample.Criteria criteria = example.createCriteria();
                criteria.andUidEqualTo(uid);
                List<Collect> collectList = collectMapper.selectByExample(example);
                PositionExample positionExample = new PositionExample();

                if (!collectList.isEmpty()) {
                    for (Collect collect : collectList) {
                        PositionExample.Criteria criteria1 = positionExample.createCriteria();
                        criteria1.andPidEqualTo(collect.getPid());
                        positionExample.or(criteria1);
                    }
                    positions = positionMapper.selectByExample(positionExample);
                    redisUtil.setCollect(String.valueOf(uid), positions);
                }
            }
        }

        return positions;
    }

    @Override
    public String addCollection(int uid, int pid) {

        Collect collect = new Collect();
        CollectExample example = new CollectExample();
        CollectExample.Criteria criteria = example.createCriteria();
        if (StringUtils.isNotBlank(String.valueOf(uid)) && StringUtils.isNotBlank(String.valueOf(pid))) {
            criteria.andUidEqualTo(uid);
            criteria.andPidEqualTo(pid);
            List<Collect> collects = collectMapper.selectByExample(example);
            redisUtil.delCollect(String.valueOf(uid));
//            如果已经存在就不执行
            if (collects.isEmpty() || collects == null) {
                collect.setUid(uid);
                collect.setPid(pid);
                collectMapper.insert(collect);
            }
            return "success";
        } else {
            LOGGER.error("add collection failed uid:{},pid:{}", uid, pid);
            return "fail";
        }

    }

    @Override
    public String deleteCollection(int uid, int pid) {

        CollectExample collectExample = new CollectExample();
        CollectExample.Criteria criteria = collectExample.createCriteria();
        if (StringUtils.isNotBlank(String.valueOf(uid)) && StringUtils.isNotBlank(String.valueOf(pid))) {
            criteria.andPidEqualTo(pid)
                    .andUidEqualTo(uid);
            collectMapper.deleteByExample(collectExample);
            redisUtil.delCollect(String.valueOf(uid));
            return "success";
        } else {
            LOGGER.error("cancel collection failed uid:{},pid:{}", uid, pid);
            return "fail";
        }
    }

    @Override
    public boolean isCollected(int uid, int pid) {

        CollectExample collectExample = new CollectExample();
        CollectExample.Criteria criteria = collectExample.createCriteria();
        if (StringUtils.isNotBlank(String.valueOf(uid)) && StringUtils.isNotBlank(String.valueOf(pid))) {
            criteria.andPidEqualTo(pid)
                    .andUidEqualTo(uid);
            List<Collect> collect = collectMapper.selectByExample(collectExample);
            if (collect.isEmpty()) {
                return false;
            } else {
                return true;
            }
        } else {
            LOGGER.error("cancel collection failed uid:{},pid:{}", uid, pid);
            return false;
        }
    }
}
