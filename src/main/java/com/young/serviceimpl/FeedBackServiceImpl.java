package com.young.serviceimpl;

import com.young.mapper.FeedBackMapper;
import com.young.pojo.FeedBack;
import com.young.pojo.User;
import com.young.service.FeedBackService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * 提供意见反馈服务
 *
 * @author 吕圣业
 * @since 2 六月 2019
 */
@Service("feedBackService")
public class FeedBackServiceImpl implements FeedBackService {

    private static final Logger LOGGER = LoggerFactory.getLogger(FeedBackServiceImpl.class);

    @Resource
    FeedBackMapper feedBackMapper;

    @Override
    public String addSuggestion(FeedBack feedBack) {

        if(StringUtils.isNotBlank(feedBack.getUid().toString())&&StringUtils.isNotBlank(feedBack.getContent())){
            feedBack.setUid(feedBack.getUid());
            feedBackMapper.insert(feedBack);
            return "success";
        }else {
            LOGGER.error("feedback add fail feedBack:{}",feedBack);
            return "fail";
        }
    }
}
