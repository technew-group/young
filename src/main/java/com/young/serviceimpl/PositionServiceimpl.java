package com.young.serviceimpl;

import com.young.mapper.PositionMapper;
import com.young.pojo.Position;
import com.young.service.PositionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author 张龙龙
 * @date 2019/6/2 19:41
 */
@Service("positionService")
public class PositionServiceimpl implements PositionService {

    /**
     *
     */
    @Resource
    PositionMapper positionMapper;

    @Value("${my.mysql.pageSize}")
    int pageSize;


    @Override
    public List<Position> getPositionListForPage(Integer page) {

        int count=this.getCount();
        if(page==null||page<1){
            page=1;
        }
        if(page>(count/pageSize)+1){
            return null;
        }
        Map<String,Integer> map=new HashMap<String,Integer>();
        map.put("page",(page-1)*pageSize);
        map.put("pageSize",pageSize);
        return  positionMapper.getPositionListForPage(map);
    }

    @Override
    public Position getPosition(int pid) {
        return positionMapper.selectByPrimaryKey(pid);
    }

    @Override
    public int getCount() {
        return positionMapper.countByExample(null);
    }

    @Override
    public List<Position> getPositionOfSearch() {
        return positionMapper.getPositionOfSearch();
    }
}
