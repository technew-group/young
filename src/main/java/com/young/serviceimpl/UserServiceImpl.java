package com.young.serviceimpl;

import com.young.mapper.UserMapper;
import com.young.mapper.Work_experienceMapper;
import com.young.pojo.User;
import com.young.pojo.UserExample;
import com.young.pojo.Work_experience;
import com.young.pojo.Work_experienceExample;
import com.young.service.UserService;
import com.young.util.DateUtil;
import com.young.util.RedisUtil;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 提供个人信息页面的服务
 *
 * @author 吕圣业
 * @since 2 六月 2019
 */
@Service("userService")
public class UserServiceImpl implements UserService {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserServiceImpl.class);

    @Resource
    UserMapper userMapper;
    @Resource
    Work_experienceMapper experienceMapper;
    @Resource
    RedisUtil redisUtil;

    @Override
    public List<User> getUserList() {
        return null;
    }

    @Override
    public User getUserByUid(int uid) {
        User res=redisUtil.getUser(String.valueOf(uid));
        if(null==res){
            res=userMapper.selectByPrimaryKey(uid);
            redisUtil.setUser(String.valueOf(uid),res);
        }
        return res;
    }

    @Override
    public User userAccess(String openid,String ucity) {

        User user = getUserByOpenid(openid);
        if (null == user) {
            user = new User();
            user.setOpenid(openid);
            //设置所在城市
            user.setUcity(ucity);
            addUser(user);
        }
        return user;
    }

    @Override
    public void addUser(User user) {

        if (StringUtils.isNotBlank(user.getOpenid())) {
            userMapper.insert(user);
            redisUtil.setUser(String.valueOf(user.getUid()),user);
        } else {
            LOGGER.error("get openid failed when addUser!");
        }
    }

    @Override
    public User getUserByOpenid(String openid) {

        UserExample example = new UserExample();
        UserExample.Criteria criteria = example.createCriteria();
        List<User> users = null;
        if (StringUtils.isNotBlank(openid)) {
            criteria.andOpenidEqualTo(openid);
            users = userMapper.selectByExampleWithBLOBs(example);
        }

        if (null != users && !users.isEmpty()) {
            User user = users.get(0);
            redisUtil.setUser(String.valueOf(user.getUid()),user);
            List<Work_experience> workList = user.getWorkList();
            //格式化工作经历
            for (Work_experience experience : workList) {
                String begin = DateUtil.toShowDate(experience.getBeginTime());
                String end = DateUtil.toShowDate(experience.getEndTime());
                experience.setBeginTime(begin);
                experience.setEndTime(end);
            }
            return user;
        } else {
            return null;
        }
    }

    @Override
    public String updateBasicInformation(User user) {

        if (StringUtils.isNotBlank(user.getOpenid())) {
            if(StringUtils.isNotBlank(user.getUbirthday())){
                user.setUbirthday(user.getUbirthday().trim());
            }
            UserExample example = new UserExample();
            UserExample.Criteria criteria = example.createCriteria();
            criteria.andOpenidEqualTo(user.getOpenid());
            userMapper.updateByExampleWithBLOBs(user, example);
            redisUtil.delUser(String.valueOf(user.getUid()));
            return "success";
        } else {
            LOGGER.error("fail to update information,openid is null");
            return "fail";
        }
    }

    @Override
    public String updateExperience(Work_experience experience) {

        Integer id = experience.getId();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        Date begin = null;
        Date end = null;
        try {
            begin = formatter.parse(experience.getBeginTime());
            end = formatter.parse(experience.getEndTime());
        } catch (ParseException e) {
            LOGGER.error("experience update fail , because date change fail experience:{}", experience);
            e.printStackTrace();
        }
        if (begin.compareTo(end) > 0) {
            LOGGER.error("experience update fail ,because date is unsuitable experience:{}", experience);
            return "fail";
        }
        //有id则说明是修改
        if (null != id && StringUtils.isNotBlank(id.toString())) {
            Work_experienceExample experienceExample = new Work_experienceExample();
            Work_experienceExample.Criteria criteria1 = experienceExample.createCriteria();
            criteria1.andIdEqualTo(id);
            experienceMapper.updateByExampleWithBLOBs(experience, experienceExample);
        } else {
            //否则插入经历介绍
            experienceMapper.insert(experience);
        }
        redisUtil.delUser(String.valueOf(experience.getUid()));
        return "success";
    }

    @Override
    public String deleteExperience(int id,int uid) {

        int res = experienceMapper.deleteByPrimaryKey(id);
        redisUtil.delUser(String.valueOf(uid));
        if (res == 0) {
            LOGGER.error("fail to delete experience id:{}", id);
            return "fail";
        } else {
            return "success";
        }

    }

    @Override
    public List<Work_experience> getWorkExperienceByUid(int uid) {

        List<Work_experience> experiences=new ArrayList<>();
        if(StringUtils.isNotBlank(String.valueOf(uid))){
            experiences=experienceMapper.selectByUid(uid);
            return experiences;
        }else{
            LOGGER.error("fail to get uid:{}", uid);
            return experiences;
        }

    }


}
