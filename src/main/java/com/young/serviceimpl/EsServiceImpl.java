package com.young.serviceimpl;

import com.alibaba.fastjson.JSONObject;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.young.service.EsService;
import org.apache.lucene.search.join.ScoreMode;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.Client;
import org.elasticsearch.index.query.*;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author 张龙龙
 * @date 2019/6/1 21:18
 */

@Service("esService")
public class EsServiceImpl  implements EsService {

    @Autowired
    ElasticsearchTemplate elasticsearchTemplate;


    /**
     * @param param
     * @return
     *
     * 通过输入的 （公司名\职位名\ 地址） 的参数
     * 查找职位信息
     */
    @Override
    public  List<JSONObject> getPosition(String param) throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        Client client = elasticsearchTemplate.getClient();
        MultiMatchQueryBuilder positionBuilder= QueryBuilders.multiMatchQuery(param,"workAddress","positionName");

        MatchQueryBuilder companyQuery = QueryBuilders.matchQuery("company.cName", param);
        NestedQueryBuilder company = (NestedQueryBuilder) QueryBuilders.nestedQuery("company",companyQuery, ScoreMode.None);


        BoolQueryBuilder boolQueryBuilder = QueryBuilders.boolQuery();
        List<QueryBuilder> should = boolQueryBuilder.should();
        should.add(positionBuilder);
        should.add(company);

        SearchResponse searchResponse = client.prepareSearch("position")
                .setQuery(boolQueryBuilder)
                .get();
        SearchHits hits = searchResponse.getHits();
        if(hits==null||hits.getTotalHits()==0){
            return null;
        }
        List<JSONObject> list=new ArrayList<JSONObject>();
        for (SearchHit hit:hits) {
            JSONObject object=JSONObject.parseObject(hit.getSourceAsString());
            list.add(object);
        }
        return list;
    }

}
