package com.young.serviceimpl;

import com.young.mapper.CompanyMapper;
import com.young.pojo.Company;
import com.young.service.CompanyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author 张龙龙
 * @date 2019/6/2 20:44
 */
@Service("companyService")
public class CompanyServiceImpl implements CompanyService {
    @Autowired
    CompanyMapper companyMapper;

    @Override
    public Company getCompanyById(int cid) {
        return companyMapper.selectByPrimaryKey(cid);
    }
}
