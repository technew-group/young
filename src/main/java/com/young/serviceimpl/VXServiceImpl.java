package com.young.serviceimpl;

import com.alibaba.fastjson.JSONObject;
import com.young.service.VXService;
import com.young.util.AesCbcUtil;
import com.young.util.ConstantUtil;
import com.young.util.HttpUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

/**
 * 封装微信接口
 *
 * @author 吕圣业
 * @since 3 六月 2019
 */
@Service("vxService")
public class VXServiceImpl implements VXService {

    private static final Logger LOGGER = LoggerFactory.getLogger(VXServiceImpl.class);

    @Override
    public String getSessionKey(String code){

        // 小程序唯一标识 (在微信小程序管理后台获取)
        String wxspAppid = ConstantUtil.APPID;
        // 小程序的 app secret (在微信小程序管理后台获取)
        String wxspSecret = ConstantUtil.SECRET;
        // 授权（必填）
        String grant_type = "authorization_code";

        //向微信服务器 使用登录凭证 code 获取 session_key 和 openid
        // 请求参数
        String params = "appid=" + wxspAppid + "&secret=" + wxspSecret + "&js_code=" + code + "&grant_type="
                + grant_type;
        // 发送请求
        String sr = HttpUtil.sendGet("https://api.weixin.qq.com/sns/jscode2session", params);
        return sr;
    }
    @Override
    public Map getUserInfo(String encryptedData, String iv, String code){

        Map map = new HashMap();
        // 登录凭证不能为空
        if (code == null || code.length() == 0) {
            map.put("status", 0);
            map.put("msg", "code 不能为空");
            return map;
        }

        String str=getSessionKey(code);
        // 解析相应内容（转换成json对象）
        JSONObject json = JSONObject.parseObject(str);
        // 获取会话密钥（session_key）
        String session_key = json.get("session_key").toString();
        // 用户的唯一标识（openid）
        String openid = (String) json.get("openid");

        // 对encryptedData加密数据进行AES解密
        try {
            String result = AesCbcUtil.decrypt(encryptedData, session_key, iv, "UTF-8");
            if (null != result && result.length() > 0) {
                map.put("status", 1);
                map.put("msg", "解密成功");

                JSONObject userInfoJSON = JSONObject.parseObject(result);
                Map userInfo = new HashMap();
                // 解密unionId & openId;
                userInfo.put("openId", userInfoJSON.get("openId"));
                userInfo.put("nickName", userInfoJSON.get("nickName"));
                userInfo.put("gender", userInfoJSON.get("gender"));
                userInfo.put("city", userInfoJSON.get("city"));
                userInfo.put("province", userInfoJSON.get("province"));
                userInfo.put("country", userInfoJSON.get("country"));
                userInfo.put("avatarUrl", userInfoJSON.get("avatarUrl"));
                userInfo.put("unionId", userInfoJSON.get("unionId"));
                map.put("userInfo", userInfo);
            } else {
                map.put("status", 0);
                map.put("msg", "解密失败");
                LOGGER.error("encryptedData fail msg");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return map;
    }

    @Override
    public String getTelNum(String encryptedData, String iv, String code){

        String str=getSessionKey(code);
        // 解析相应内容（转换成json对象）
        JSONObject json = JSONObject.parseObject(str);
        // 获取会话密钥（session_key）
        String session_key = json.get("session_key").toString();

        try {
            String result = AesCbcUtil.decrypt(encryptedData, session_key, iv, "UTF-8");
            System.out.println(result);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }
}
