package com.young.serviceimpl;

import com.young.mapper.EnrollMapper;
import com.young.pojo.*;
import com.young.service.EnrollService;
import com.young.util.RedisUtil;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * 提供我的报名，报名等服务
 *
 * @author 吕圣业
 * @since 2 六月 2019
 */
@Service("enrollService")
public class EnrollServiceImpl implements EnrollService {

    private static final Logger LOGGER = LoggerFactory.getLogger(EnrollServiceImpl.class);
    @Resource
    private EnrollMapper enrollMapper;
    @Resource
    private RedisUtil redisUtil;

    @Override
    public List<Enroll> getAllDeliverByUid(int uid) {

        List<Enroll> enrolls=new ArrayList<>();

        if(StringUtils.isNotBlank(String.valueOf(uid))){
            enrolls=redisUtil.getEnroll(String.valueOf("allDeliver"+uid));
            if(null==enrolls){
                EnrollExample example=new EnrollExample();
                EnrollExample.Criteria criteria=example.createCriteria();
                criteria.andUidEqualTo(uid);
                enrolls=enrollMapper.selectByExample(example);
                redisUtil.setEnroll(String.valueOf("allDeliver"+uid),enrolls);
            }
        }
        return enrolls;
    }

    @Override
    public List<Enroll> getSawByUid(int uid) {

        List<Enroll> enrolls=new ArrayList<>();

        if(StringUtils.isNotBlank(String.valueOf(uid))){
            enrolls=redisUtil.getEnroll(String.valueOf("saw"+uid));
            if(null==enrolls){
                EnrollExample example=new EnrollExample();
                EnrollExample.Criteria criteria=example.createCriteria();
                criteria.andUidEqualTo(uid)
                        .andStatusNotEqualTo("已投递");
                enrolls=enrollMapper.selectByExample(example);
                redisUtil.setEnroll(String.valueOf("saw"+uid),enrolls);
            }
        }

        return enrolls;
    }

    @Override
    public String addEnroll(int pid,int uid) {

        if(StringUtils.isNotBlank(String.valueOf(pid))&&StringUtils.isNotBlank(String.valueOf(uid))){
            Enroll enroll=new Enroll();
            enroll.setPid(pid);
            enroll.setUid(uid);
            enroll.setStatus("已投递");
            enrollMapper.insert(enroll);
            redisUtil.delEnroll(String.valueOf("allDeliver"+uid));
            redisUtil.delEnroll(String.valueOf("saw"+uid));
            return "success";
        } else {
            LOGGER.error("add enroll failed ! uid:{},pid:{}",uid,pid);
            return "fail";
        }
    }

    @Override
    public boolean isEnrolled(int pid, int uid) {

        EnrollExample example=new EnrollExample();
        EnrollExample.Criteria criteria=example.createCriteria();
        if(StringUtils.isNotBlank(String.valueOf(pid))&&StringUtils.isNotBlank(String.valueOf(uid))){
            criteria.andPidEqualTo(pid)
                    .andUidEqualTo(uid);
            List<Enroll> enroll=enrollMapper.selectByExample(example);
            if(enroll.isEmpty()){
                return false;
            }else {
                return true;
            }
        }
        LOGGER.error("isEnrolled failed uid:{},pid:{}", uid,pid);
        return false;
    }
}
