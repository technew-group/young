package com.young.pojo;

import java.util.List;
import lombok.Data;


@Data
public class User {
    private Integer uid;

    private String uname;

    private String usex;

    private String ubirthday;

    private String uschoolName;

    private String uschoolProfession;

    private String utel;

    private String uemail;

    private String openid;

    private String ucity;

    private String uspecialty;

    private List<Work_experience> workList;

     private List<Label> labelList;

    @Override
    public String toString() {
        return "User{" +
                "uid=" + uid +
                ", uname='" + uname + '\'' +
                ", usex='" + usex + '\'' +
                ", ubirthday='" + ubirthday + '\'' +
                ", uschoolName='" + uschoolName + '\'' +
                ", uschoolProfession='" + uschoolProfession + '\'' +
                ", utel='" + utel + '\'' +
                ", uemail='" + uemail + '\'' +
                ", openid='" + openid + '\'' +
                ", ucity='" + ucity + '\'' +
                ", uspecialty='" + uspecialty + '\'' +
                ", workList=" + workList +
                ", labelList=" + labelList +
                '}';
    }
}