package com.young.pojo;

import lombok.Data;


@Data
public class Enroll {

    private Integer id;

    private Integer uid;

    private Integer pid;

    private Position position;

    private String status;

    @Override
    public String toString() {
        return "Enroll{" +
                "id=" + id +
                ", uid=" + uid +
                ", pid=" + pid +
                ", position=" + position +
                ", status='" + status + '\'' +
                '}';
    }
}