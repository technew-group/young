package com.young.pojo;
import lombok.Data;


@Data
public class Work_experience {
    private Integer id;

    private String beginTime;

    private String endTime;

    private String job;

    private Integer uid;

    private String workDescribe;

    public Integer getId() {
        return id;
    }

    @Override
    public String toString() {
        return "Work_experience{" +
                "id=" + id +
                ", beginTime='" + beginTime + '\'' +
                ", endTime='" + endTime + '\'' +
                ", job='" + job + '\'' +
                ", uid=" + uid +
                ", workDescribe='" + workDescribe + '\'' +
                '}';
    }
}