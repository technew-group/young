package com.young.pojo;

import lombok.Data;

@Data
public class Label {

    private Integer id;

    private String describes;

    private Integer uid;


    @Override
    public String toString() {
        return "Label{" +
                "id=" + id +
                ", describes='" + describes + '\'' +
                ", uid=" + uid +
                '}';
    }
}