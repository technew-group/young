package com.young.pojo;

import lombok.Data;


@Data
public class Company {

    private Integer cid;

    private String clogo;

    private String companyName;

    private String companyAddress;

    private String companyIntroduce;
}