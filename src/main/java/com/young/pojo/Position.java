package com.young.pojo;

import lombok.Data;


@Data
public class Position {

    private Integer pid;

    private String salary;

    private String workAddress;

    private String positionName;

    private String workRequirement;

    private String workContent;

    private String beginTime;

    private String endtime;

    private Integer cid;

    private Company company;
}