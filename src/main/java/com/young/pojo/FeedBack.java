package com.young.pojo;

import lombok.Data;

@Data
public class FeedBack {

    private Integer id;

    private Integer uid;

    private String content;

}