package com.young.pojo;

import lombok.Data;

@Data
public class Collect {

    private Integer id;

    private Integer uid;

    private Integer pid;

}