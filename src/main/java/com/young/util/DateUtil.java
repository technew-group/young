package com.young.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 日期工具类
 *
 * @author 吕圣业
 * @since 7 六月 2019
 */
public class DateUtil {

    private static final Logger LOGGER = LoggerFactory.getLogger(DateUtil.class);

    /**
     * 把yyyy-MM-dd转换成yyy.MM
     * @param dateStr
     */
    public static final String toShowDate(String dateStr){
        SimpleDateFormat formatter=new SimpleDateFormat("yyyy-MM");
        Date begin=null;
        try {
            begin=formatter.parse(dateStr);
        } catch (ParseException e) {
            LOGGER.error("date change fail from yyyy-MM-dd to yyyy-MM");
            e.printStackTrace();
        }
        String showStr=(new SimpleDateFormat("yyyy-MM")).format(begin);
        showStr=showStr.replace( '-','.' );
        return showStr;
    }
}
