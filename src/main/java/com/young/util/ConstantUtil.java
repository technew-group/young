package com.young.util;

/**
 * 设定全局常量
 */
public class ConstantUtil {

    //微信公众号标识
    public  static final String  APPID="wxed46f688a8b1cd29";
    //微信公众号密码
    public  static final String  SECRET="bc7b88a7584f9f271b846d36a49f41f4";

    //配置服务器所使用token
    public  static final String   ACCESS_TOKEN="weixin";

    //http域名
    public  static final  String   HTTPURL="http://ayoniusth.com/web";
    //https域名
    public  static final  String   HTTPSURL="http://ayoniusth.com/web";
    //自设的token
    public  static final  String   MYTOKEN="zhangll";
    //成绩消息模板
    public  static  final String  TEMPLATE_ID_GRADE="2P8e8CiUVhsAetHUUOdYacFRn_xu6BlhxWs1HMfPqL8";
    //活动消息模板
    public  static final  String  TEMPLATE_ID_ACTIVITY="znBeeZuQXQxoO-n59NwVNIntMu-yQPUOt_N6lv97Kcg";
    //成绩推送模板
    public  static final  String TEMPLATE_ID_COURSE="UtFuWepqt7j0s4O-2mPJEeu9ZvKgkryH67GkRTMWevw";
    //管理员的openid
    public  static final  String   MANAGET_OPENID="oWs3z1DOhfylVm6XZEsP8r8ibikI";
}
