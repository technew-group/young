package com.young.util;

import com.young.pojo.Enroll;
import com.young.pojo.Position;
import com.young.pojo.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * 封装redis接口
 *
 * @author 吕圣业
 * @since 1 六月 2019
 */
@Component("redisUtil")
public class RedisUtil {

    @Autowired
    private RedisTemplate<String,Object> redisTemplate;

    public void setUser(String uid, User value) {
        redisTemplate.opsForValue().set("uid"+uid,value, 60 * 10, TimeUnit.SECONDS);
    }

    public User getUser(String uid) {
        redisTemplate.expire("uid"+uid,60 * 10, TimeUnit.SECONDS);
        return (User)redisTemplate.opsForValue().get("uid"+uid);
    }

    public void delUser(String uid){
        redisTemplate.delete("uid"+uid);
    }

    public void setEnroll(String uid, List<Enroll> enrolls) {
        redisTemplate.opsForValue().set("eid"+uid,enrolls, 60 * 10, TimeUnit.SECONDS);
    }

    public List<Enroll> getEnroll(String uid) {
        redisTemplate.expire("enroll"+uid,60 * 10, TimeUnit.SECONDS);
        return (List<Enroll>)redisTemplate.opsForValue().get("eid"+uid);
    }

    public void delEnroll(String uid){
        redisTemplate.delete("eid"+uid);
    }

    public void setCollect(String uid, List<Position> positions) {
        redisTemplate.opsForValue().set("collect"+uid,positions, 60 * 10, TimeUnit.SECONDS);
    }

    public List<Position> getCollect(String uid) {
        redisTemplate.expire("collect"+uid,60 * 10, TimeUnit.SECONDS);
        return (List<Position>)redisTemplate.opsForValue().get("eid"+uid);
    }

    public void delCollect(String uid){
        redisTemplate.delete("collect"+uid);
    }

}
