package com.young.biz;

import com.young.pojo.User;
import com.young.service.UserService;
import com.young.service.VXService;
import com.young.util.RedisUtil;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Map;

/**
 * 封装复杂的用户逻辑
 *
 * @author 吕圣业
 * @since 3 六月 2019
 */
@Service("userInfoBiz")
public class UserInfoBiz {

    @Resource
    private VXService vxService;
    @Resource
    private UserService userService;

    /**
     * 获取用户信息时，若用户不在库中，则将openid插入
     * @param encryptedData
     * @param iv
     * @param code
     * @return
     */
    public Map getUserInfoAndCheck(String encryptedData, String iv, String code){

        Map map=vxService.getUserInfo(encryptedData,iv,code);
        if((int)map.get("status")==1){
            Map userInfo= (Map) map.get("userInfo");
            String openid=(String) userInfo.get("openId");
            String ucity=(String) userInfo.get("city");
            User user=userService.userAccess(openid,ucity);
            map.put("basicUser",user);
        }
        return map;
    }
}
