package com.young.mapper;

import com.young.pojo.Position;
import com.young.pojo.PositionExample;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

public interface PositionMapper {

    List<Position> getPositionListForPage(Map map);

    int countByExample(PositionExample example);

    int deleteByExample(PositionExample example);

    int deleteByPrimaryKey(Integer pid);

    int insert(Position record);

    int insertSelective(Position record);

    List<Position> selectByExample(PositionExample example);

    Position selectByPrimaryKey(Integer pid);

    int updateByExampleSelective(@Param("record") Position record, @Param("example") PositionExample example);

    int updateByExample(@Param("record") Position record, @Param("example") PositionExample example);

    int updateByPrimaryKeySelective(Position record);

    int updateByPrimaryKey(Position record);

    List<Position> getPositionOfSearch();
}