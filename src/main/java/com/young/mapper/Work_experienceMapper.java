package com.young.mapper;

import com.young.pojo.Work_experience;
import com.young.pojo.Work_experienceExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface Work_experienceMapper {
    int countByExample(Work_experienceExample example);

    int deleteByExample(Work_experienceExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(Work_experience record);

    int insertSelective(Work_experience record);

    List<Work_experience> selectByExampleWithBLOBs(Work_experienceExample example);

    List<Work_experience> selectByUid(Integer uid);

    List<Work_experience> selectByExample(Work_experienceExample example);

    Work_experience selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") Work_experience record, @Param("example") Work_experienceExample example);

    int updateByExampleWithBLOBs(@Param("record") Work_experience record, @Param("example") Work_experienceExample example);

    int updateByExample(@Param("record") Work_experience record, @Param("example") Work_experienceExample example);

    int updateByPrimaryKeySelective(Work_experience record);

    int updateByPrimaryKeyWithBLOBs(Work_experience record);

    int updateByPrimaryKey(Work_experience record);
}