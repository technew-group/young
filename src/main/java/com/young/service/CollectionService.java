package com.young.service;

import com.young.pojo.Position;

import java.util.List;

/**
 * 提供我的收藏的服务
 *
 * @author 吕圣业
 * @since 2 六月 2019
 */
public interface CollectionService {

    /**
     * 根据uid取当前用户的所有收藏的职位
     * @param uid
     * @return
     */
    List<Position> getAllCollectionByUid(int uid);

    /**
     * 用户是否已收藏过该职位
     * @param uid
     * @param pid
     * @return
     */
    boolean isCollected(int uid,int pid);

    /**
     * 添加收藏
     * @param uid
     * @param pid
     * @return
     */
    String addCollection(int uid,int pid);

    /**
     * 取消收藏
     * @param uid
     * @param pid
     * @return
     */
    String deleteCollection(int uid,int pid);
}
