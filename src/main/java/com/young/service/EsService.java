package com.young.service;

import com.alibaba.fastjson.JSONObject;
import com.young.pojo.Position;

import java.io.IOException;
import java.util.List;

/**
 * @author 张龙龙
 * @date 2019/6/1 21:16
 *
 *  ES 服务类  用来提供es搜索的服务
 */
public interface EsService {

    /**
     * @param param
     * @return
     *
     * 通过条件查找信息  ( 公司名 \ 职位名 \ 地址名)
     */
    public List<JSONObject>  getPosition(String param) throws IOException;
}
