package com.young.service;

import com.young.pojo.Company;

/**
 * @author 张龙龙
 * @date 2019/6/2 20:42
 *
 *  company服务类
 */
public interface CompanyService {

    /**
     * @param cid
     * @return
     *
     *
     * 通过公司的id号 查找公司对象
     */
    Company  getCompanyById(int cid);
}
