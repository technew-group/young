package com.young.service;

import com.young.pojo.Position;

import java.util.List;

/**
 * @author 张龙龙
 * @date 2019/6/2 19:39
 */
public interface PositionService {

    /**
     * @param page
     * @return
     *
     * 此方法 查询营地信息(分页  默认大小为8  可以在application.yml中修改默认大小)
     */
    List<Position> getPositionListForPage(Integer page);

    /**
     * @param pid
     * @return
     *
     * 此方法 查询营地的详细信息
     */
    Position getPosition(int pid);

    /**
     * @return
     *
     *
     * 数据库中所有营地数据的总量
     */
    int getCount();


    /**
     *
     * @return
     *
     * 此方法 查询报名人数最多的8条营地信息
     */
    List<Position> getPositionOfSearch();
}
