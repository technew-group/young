package com.young.service;


import com.young.pojo.FeedBack;
import com.young.pojo.User;

/**
 * 提供意见反馈页面的服务
 *
 * @author 吕圣业
 * @since 2 六月 2019
 */
public interface FeedBackService {

    String addSuggestion(FeedBack feedBack);
}
