package com.young.service;

import com.young.pojo.Enroll;

import java.util.List;

/**
 * 提供我的报名页面的服务
 *
 * @author 吕圣业
 * @since 2 六月 2019
 */
public interface EnrollService {

    /**
     * 获取用户所有报名数据
     * @param uid
     * @return
     */
    List<Enroll> getAllDeliverByUid(int uid);

    /**
     * 获取营地已看的报名数据
     * @param uid
     * @return
     */
    List<Enroll> getSawByUid(int uid);

    /**
     * 将报名的职位添加到报名表
     * @param pid
     * @param uid
     * @return
     */
    String addEnroll(int pid, int uid);

    /**
     * 用户是否已报名该职位
     * @param pid
     * @param uid
     * @return
     */
    boolean isEnrolled(int pid, int uid);

}
