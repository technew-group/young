package com.young.service;

import com.young.pojo.User;
import com.young.pojo.Work_experience;

import java.util.List;


/**
 * 提供针对用户的服务
 *
 * @author 吕圣业
 * @since 2 六月 2019
 */

public interface UserService {
    /**
     * @return UserList
     *
     * 返回数据库中用户表的所有信息
     */
    List<User> getUserList();

    /**
     * @param uid
     * @return User
     *
     * 通过主键uid 返回用户信息
     */
    User getUserByUid(int uid);

    /**
     * 刚进入我的简历页面时调用
     * @param user
     */
    void addUser(User user);

    User getUserByOpenid(String openid);

    /**
     * 判断用户是否存在，不存在则插入
     * @return
     */
    User userAccess(String openid,String ucity);

    /**
     * 更新个人信息或特长爱好时调用
     * @return
     */
    String updateBasicInformation(User user);

    /**
     * 更新工作经历,暂时就存一条。有则覆盖，没有则插入
     * @param experience
     * @return
     */
    String updateExperience(Work_experience experience);

    /**
     * 删除本条工作经历
     * @param id
     * @return
     */
    String deleteExperience(int id,int uid);

    /**
     * 获取当前用户的工作经历
     * @param uid
     * @return
     */
    List<Work_experience> getWorkExperienceByUid(int uid);
}
