package com.young.service;

import java.util.Map;

/**
 * 封装微信接口
 *
 * @author 吕圣业
 * @since 2 六月 2019
 */
public interface VXService {

    /**
     * 通过code获取sessionKey
     * @param code
     * @return
     */
    String getSessionKey(String code);
    /**
     * 获取用户微信信息
     * @param encryptedData 明文,加密数据
     * @param iv   加密算法的初始向量
     * @param code  用户允许登录后，回调内容会带上 code（有效期五分钟），
     *              开发者需要将 code 发送到开发者服务器后台，
     *              使用code 换取 session_key api，将 code 换成 openid 和 session_key
     * @return
     */
    Map getUserInfo(String encryptedData, String iv, String code);

    /**
     * 获取用户手机号，权限不够，没写完
     * @param encryptedData
     * @param iv
     * @param code
     * @return
     */
    String getTelNum(String encryptedData, String iv, String code);
}
